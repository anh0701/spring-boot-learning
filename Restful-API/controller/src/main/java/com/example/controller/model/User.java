package com.example.controller.model;

import jakarta.persistence.*;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = "_user")
public class User {
    @Id
    @Column(name = "ID")
    @GeneratedValue
    private long ID;

    @Column(name = "name")
    private String name;

    @Column(name = "age")
    private int age;
}
