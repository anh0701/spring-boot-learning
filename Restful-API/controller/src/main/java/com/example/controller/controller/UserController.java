package com.example.controller.controller;

import com.example.controller.Repository.UserRepository;
import com.example.controller.model.User;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@AllArgsConstructor
public class UserController {
    private UserRepository userRepo;
    @GetMapping("/")
    public ResponseEntity<?> index(){
        return ResponseEntity.ok(userRepo.findAll());
    }

    @PostMapping("/add-user")
    public ResponseEntity<?> addUser(@RequestBody User user){
        userRepo.save(user);
        return ResponseEntity.ok("Success");
    }

//    @PutMapping("/update-user/{id}")
//    public ResponseEntity<?> updateUser(@RequestBody User user, @PathVariable String id){
//        return ResponseEntity.ok("");
//    }
//
    @DeleteMapping("/delete-user/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable("id") long id){
        userRepo.deleteById(id);
        return ResponseEntity.ok("delete success");
    }
}
