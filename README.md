# spring boot learning

## Run project

- step 1: cd folder config
    ```sh
        cd .\config\
    ```

- step 2: check container running
    ```sh
        docker ps
    ```

- step 3: if container running,  stop container
    ```sh
        docker stop [container id]
    ```

- step 4: run docker
    ```sh
        docker-compose up -d
    ```

- step 5: run file ControllerApplication.java

- step 6: run request in file test-HTTP-Req.http

- step 7: stop docker 
    ```sh
        docker-compose down
    ```